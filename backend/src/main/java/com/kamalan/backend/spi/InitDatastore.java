package com.kamalan.backend.spi;

import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.kamalan.backend.model.ConferenceForm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Hesam on 03/01/15
 */
public class InitDatastore
{
    private String[] confName = {"Conf 1", "Conf 2", "Conf 3", "Conf 4", "Conf 5"};
    private String[] confDescription = {"Desc 1", "Desc 2", "Desc 3", "Desc 4", "Desc 5", };
    private String[] confTopics = {"Topic 1", "Topic 2", "Topic 3", "Topic 4", "Topic 5"};
    private String[] confCity = {"London", "Paris", "London", "Chicago", "London"};
    private String[] confStartDate = {
            "2015-01-01T00:00:00.000+0800",
            "2015-01-04T00:00:00.000+0800",
            "2015-01-15T00:00:00.000+0800",
            "2015-02-01T00:00:00.000+0800",
            "2015-02-02T00:00:00.000+0800"
    };
    private String[] confStopDate = {
            "2015-01-07T00:00:00.000+0800",
            "2015-01-07T00:00:00.000+0800",
            "2015-01-25T00:00:00.000+0800",
            "2015-02-07T00:00:00.000+0800",
            "2015-02-10T00:00:00.000+0800"
    };
    private int[] confMaxAttendees = {7, 10, 20, 30, 40};


    private User user;


    InitDatastore(User user)
    {
        this.user = user;
    }

    public void initDataStore() throws ParseException, UnauthorizedException
    {
        for (int i = 0; i < 5; i++)
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
            List<String> topics = new ArrayList<>(2);
            topics.add(confTopics[i]);
            topics.add("GAE");

            ConferenceForm form = new ConferenceForm(confName[i], confDescription[i], topics,
                    confCity[i], df.parse(confStartDate[i]), df.parse(confStopDate[i]), confMaxAttendees[i]);

            ConferenceApi api = new ConferenceApi();
            api.createConference(user, form);
        }
    }
}
