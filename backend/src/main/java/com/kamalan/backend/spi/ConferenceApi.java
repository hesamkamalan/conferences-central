package com.kamalan.backend.spi;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.ConflictException;
import com.google.api.server.spi.response.ForbiddenException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.Work;
import com.googlecode.objectify.cmd.Query;
import com.kamalan.backend.Constants;
import com.kamalan.backend.Utility.WrappedBoolean;
import com.kamalan.backend.model.Announcement;
import com.kamalan.backend.model.AppEngineUser;
import com.kamalan.backend.model.Conference;
import com.kamalan.backend.model.ConferenceForm;
import com.kamalan.backend.model.ConferenceQueryForm;
import com.kamalan.backend.model.Profile;
import com.kamalan.backend.model.ProfileForm;
import com.kamalan.backend.model.ProfileForm.TeeShirtSize;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import static com.kamalan.backend.service.OfyService.factory;
import static com.kamalan.backend.service.OfyService.ofy;

/**
 * Created by Hesam on 28/12/14
 */
@Api(
        name = "conferenceApi",
        version = "v1",
        scopes = {Constants.EMAIL_SCOPE},
        clientIds = {Constants.WEB_CLIENT_ID, Constants.ANDROID_CLIENT_ID,
                Constants.API_EXPLORER_CLIENT_ID},
        audiences = {Constants.ANDROID_AUDIENCE},
        description = "API for the Conference Central Backend application.")
public class ConferenceApi
{
    // TODO: this method should not be used in production mode
    @ApiMethod(name = "initDatastore", path = "initDatastore", httpMethod = HttpMethod.POST)
    public void initDatastore(final User user) throws UnauthorizedException, ParseException
    {
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        InitDatastore initDatastore = new InitDatastore(user);
        initDatastore.initDataStore();
    }

    /*
     * Get the display name from the user's email. For example, if the email is
     * lemoncake@example.com, then the display name becomes "lemoncake."
     */
    private static String extractDefaultDisplayNameFromEmail(String email)
    {
        return email == null ? null : email.substring(0, email.indexOf("@"));
    }

    /**
     * Creates or updates a Profile object associated with the given user object.
     *
     * @param user        A User object injected by the cloud endpoints.
     * @param profileForm A ProfileForm object sent from the client form.
     * @return Profile object just created.
     * @throws UnauthorizedException when the User object is null.
     */
    @ApiMethod(name = "saveProfile", path = "profile", httpMethod = HttpMethod.POST)
    public Profile saveProfile(final User user, ProfileForm profileForm)
            throws UnauthorizedException
    {
        // If the user is not logged in, throw an UnauthorizedException
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // Get the userId and mainEmail
        String mainEmail = user.getEmail();
        String userId = getUserId(user);

        // Get the displayName and teeShirtSize sent by the request.
        String displayName = profileForm.getDisplayName();
        TeeShirtSize teeShirtSize = profileForm.getTeeShirtSize();

        // Get the Profile from the datastore if it exists otherwise create a new one
        Profile profile = ofy().load().key(Key.create(Profile.class, userId)).now();
        if (profile == null)
        {
            // Populate the displayName and teeShirtSize with default values if not sent in the request
            if (displayName == null)
            {
                displayName = extractDefaultDisplayNameFromEmail(user.getEmail());
            }

            if (teeShirtSize == null)
            {
                teeShirtSize = TeeShirtSize.NOT_SPECIFIED;
            }

            // Now create a new Profile entity
            profile = new Profile(userId, displayName, mainEmail, teeShirtSize);
        }
        else
        {
            // The Profile entity already exists, update the Profile entity
            profile.update(displayName, teeShirtSize);
        }

        // Save the entity in the datastore
        ofy().save().entity(profile).now();

        // Return the profile
        return profile;
    }

    /**
     * Returns a Profile object associated with the given user object. The cloud
     * endpoints system automatically inject the User object.
     *
     * @param user A User object injected by the cloud endpoints.
     * @return Profile object.
     * @throws UnauthorizedException when the User object is null.
     */
    @ApiMethod(name = "getProfile", path = "profile", httpMethod = HttpMethod.GET)
    public Profile getProfile(final User user) throws UnauthorizedException
    {
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // load the Profile Entity
        String userId = getUserId(user);
        Key key = Key.create(Profile.class, userId);

        return (Profile) ofy().load().key(key).now();
    }

    @ApiMethod(name = "createConference", path = "conference", httpMethod = HttpMethod.POST)
    public Conference createConference(final User user, final ConferenceForm conferenceForm)
            throws UnauthorizedException
    {
        // If the user is not logged in, throw an UnauthorizedException
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // Get user id
        final String userId = getUserId(user);

        // Get the key for the user's profile
        Key<Profile> profileKey = Key.create(Profile.class, userId);

        // Allocate a key for the conference
        final Key<Conference> conferenceKey = factory().allocateId(profileKey, Conference.class);

        // Get the conference Id from the key
        final long conferenceId = conferenceKey.getId();

        // Get default queue
        final Queue queue = QueueFactory.getDefaultQueue();

        // Start transaction
        Conference conference = ofy().transact(new Work<Conference>()
        {
            @Override
            public Conference run()
            {
                // Get the existing Profile entity for the current user if there is one
                // Otherwise create a new Profile entity with default values
                Profile profile = getProfileFromUser(user);

                // Create a new Conference Entity, specifying the user's Profile entity
                // as the parent of the conference
                Conference conference = new Conference(conferenceId, userId, conferenceForm);

                // Save Conference and Profile Entities
                ofy().save().entities(conference, profile).now();

                queue.add(ofy().getTransaction(),
                        TaskOptions.Builder.withUrl("/tasks/send_confirmation_email")
                                .param("email", profile.getMainEmail())
                                .param("conferenceInfo", conference.toString()));

                return conference;
            }
        });

        return conference;
    }

    /**
     * Queries against the datastore with the given filters and returns the result.
     * <p/>
     * Normally this kind of method is supposed to get invoked by a GET HTTP method,
     * but we do it with POST, in order to receive conferenceQueryForm Object via the POST body.
     *
     * @param conferenceQueryForm A form object representing the query.
     * @return A List of Conferences that match the query.
     */
    @ApiMethod(name = "queryConferences", path = "queryConferences", httpMethod = HttpMethod.POST)
    public List<Conference> queryConferences(ConferenceQueryForm conferenceQueryForm)
    {
        //        return conferenceQueryForm.getQuery().list();
        Iterable<Conference> conferenceIterable = conferenceQueryForm.getQuery();
        List<Conference> result = new ArrayList<>(0);
        List<Key<Profile>> organizersKeyList = new ArrayList<>(0);

        for (Conference conference : conferenceIterable)
        {
            organizersKeyList.add(Key.create(Profile.class, conference.getOrganizerUserId()));
            result.add(conference);
        }

        // To avoid separate datastore gets for each Conference, pre-fetch the Profiles.
        ofy().load().keys(organizersKeyList);
        return result;
    }

    /**
     * Returns a list of Conferences that the user created.
     * In order to receive the web safe ConferenceKey via the JSON params, uses a POST method.
     *
     * @param user A user who invokes this method, null when the user is not signed in.
     * @return a list of Conferences that the user created.
     * @throws UnauthorizedException when the user is not signed in.
     */
    @ApiMethod(name = "getConferencesCreated", path = "getConferencesCreated",
            httpMethod = HttpMethod.POST)
    public List<Conference> getConferencesCreated(final User user) throws UnauthorizedException
    {
        // If not signed in, throw a 401 error.
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        String userId = getUserId(user);
        Key<Profile> userKey = Key.create(Profile.class, userId);

        return ofy().load().type(Conference.class).ancestor(userKey).order("name").list();
    }

    /**
     * Gets the Profile entity for the current user or creates it if it doesn't exist
     *
     * @param user
     * @return user's Profile
     */
    private static Profile getProfileFromUser(User user)
    {
        // First fetch the user's Profile from the datastore.
        Profile profile = ofy().load().key(Key.create(Profile.class, getUserId(user))).now();
        if (profile == null)
        {
            // Create a new Profile if it doesn't exist.
            // Use default displayName and teeShirtSize
            String email = user.getEmail();
            profile =
                    new Profile(getUserId(user), extractDefaultDisplayNameFromEmail(email), email,
                            TeeShirtSize.NOT_SPECIFIED);
        }

        return profile;
    }

    public List<Conference> filterPlayground()
    {
        Query<Conference> query = ofy().load().type(Conference.class);
        query = query.filter("city =", "London");
        query = query.filter("topics =", "Medical Innovations");
        query = query.filter("month =", 6);
        query = query.filter("maxAttendees >", 10).order("maxAttendees").order("name");

        return query.list();
    }

    /**
     * Register to attend the specified Conference.
     *
     * @param user                 An user who invokes this method, null when the user is not signed in.
     * @param websafeConferenceKey The String representation of the Conference Key.
     * @return Boolean true when success, otherwise false
     * @throws UnauthorizedException when the user is not signed in.
     * @throws NotFoundException     when there is no Conference with the given conferenceId.
     */
    @ApiMethod(
            name = "registerForConference",
            path = "conference/{websafeConferenceKey}/registration",
            httpMethod = HttpMethod.POST)
    public WrappedBoolean registerForConference(final User user,
            @Named("websafeConferenceKey") final String websafeConferenceKey)
            throws UnauthorizedException, NotFoundException, ForbiddenException, ConflictException
    {
        // If not signed in, throw a 401 error.
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // Get the userId
        final String userId = getUserId(user);

        // Start transaction
        WrappedBoolean result = ofy().transact(new Work<WrappedBoolean>()
        {
            @Override
            public WrappedBoolean run()
            {
                try
                {
                    // Get the conference key -- you can get it from websafeConferenceKey
                    // Will throw ForbiddenException if the key cannot be created
                    Key<Conference> conferenceKey = Key.create(websafeConferenceKey);

                    // Get the Conference entity from the datastore
                    Conference conference = ofy().load().key(conferenceKey).now();

                    // 404 when there is no Conference with the given conferenceId.
                    if (conference == null)
                    {
                        return new WrappedBoolean(false,
                                "No Conference found with key: " + websafeConferenceKey);
                    }

                    // Get the user's Profile entity
                    Profile profile = getProfileFromUser(user);

                    // Has the user already registered to attend this conference?
                    if (profile.getConferenceKeysToAttend().contains(websafeConferenceKey))
                    {
                        return new WrappedBoolean(false, "Already registered");
                    }
                    else if (conference.getSeatsAvailable() <= 0)
                    {
                        return new WrappedBoolean(false, "No seats available");
                    }
                    else
                    {
                        // All looks good, go ahead and book the seat

                        // Add the websafeConferenceKey to the profile's
                        // conferencesToAttend property
                        profile.addToConferenceKeysToAttend(websafeConferenceKey);

                        // Decrease the conference's seatsAvailable
                        // You can use the bookSeats() method on Conference
                        conference.bookSeats(1);

                        // Save the Conference and Profile entities
                        ofy().save().entities(profile, conference).now();

                        // We are booked!
                        return new WrappedBoolean(true, "Registration successful");
                    }
                }
                catch (Exception e)
                {
                    return new WrappedBoolean(false, "Unknown exception");
                }
            }
        });

        // if result is false
        if (!result.getResult())
        {
            if (result.getReason().contains("No Conference found with key"))
            {
                throw new NotFoundException(result.getReason());
            }
            else if (result.getReason().equals("Already registered"))
            {
                throw new ConflictException("You have already registered");
            }
            else if (result.getReason().equals("No seats available"))
            {
                throw new ConflictException("There are no seats available");
            }
            else
            {
                throw new ForbiddenException("Unknown exception");
            }
        }

        return result;
    }

    /**
     * Unregister from the specified Conference.
     *
     * @param user                 An user who invokes this method, null when the user is not signed in.
     * @param websafeConferenceKey The String representation of the Conference Key.
     * @return Boolean true when success, otherwise false
     * @throws UnauthorizedException when the user is not signed in.
     * @throws NotFoundException     when there is no Conference with the given conferenceId.
     * @throws ForbiddenException    when the request was a valid request, but the server is refusing to respond to it.
     * @throws ConflictException     when the request could not be processed because of conflict in the request.
     */
    @ApiMethod(
            name = "unregisterFromConference",
            path = "conference/{websafeConferenceKey}/registration",
            httpMethod = HttpMethod.DELETE)
    public WrappedBoolean unregisterFromConference(final User user,
            @Named("websafeConferenceKey") final String websafeConferenceKey)
            throws UnauthorizedException, NotFoundException, ForbiddenException, ConflictException
    {
        // If not signed in, throw a 401 error.
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // Start transaction
        WrappedBoolean result = ofy().transact(new Work<WrappedBoolean>()
        {
            @Override
            public WrappedBoolean run()
            {
                try
                {
                    // Get the conference key -- you can get it from websafeConferenceKey
                    // Will throw ForbiddenException if the key cannot be created
                    Key<Conference> conferenceKey = Key.create(websafeConferenceKey);

                    // Get the Conference entity from the datastore
                    Conference conference = ofy().load().key(conferenceKey).now();

                    // 404 when there is no Conference with the given conferenceId.
                    if (conference == null)
                    {
                        return new WrappedBoolean(false,
                                "No Conference found with key: " + websafeConferenceKey);
                    }

                    // Get the user's Profile entity
                    Profile profile = getProfileFromUser(user);

                    // Has the user already registered to attend this conference?
                    if (profile.getConferenceKeysToAttend().contains(websafeConferenceKey))
                    {
                        // All looks good, go ahead and book the seat
                        profile.unregisterFromConference(websafeConferenceKey);
                        conference.giveBackSeats(1);

                        // Save the Conference and Profile entities
                        ofy().save().entities(profile, conference).now();

                        // We are booked!
                        return new WrappedBoolean(true, "Unregistration successful");
                    }
                    else
                    {
                        return new WrappedBoolean(false,
                                "You have no conference with key: " + websafeConferenceKey);
                    }
                }
                catch (Exception e)
                {
                    return new WrappedBoolean(false, "Unknown exception");
                }
            }
        });

        // if result is false
        if (!result.getResult())
        {
            if (result.getReason().contains("No Conference found with key"))
            {
                throw new NotFoundException(result.getReason());
            }
            else if (result.getReason().equals("You have no conference with key"))
            {
                throw new NotFoundException(result.getReason());
            }
            else
            {
                throw new ForbiddenException("Unknown exception");
            }
        }

        return result;
    }


    /**
     * Returns a collection of Conference Object that the user is going to attend.
     *
     * @param user An user who invokes this method, null when the user is not signed in.
     * @return a Collection of Conferences that the user is going to attend.
     * @throws UnauthorizedException when the User object is null.
     */
    @ApiMethod(
            name = "getConferencesToAttend",
            path = "getConferencesToAttend",
            httpMethod = HttpMethod.GET)
    public Collection<Conference> getConferencesToAttend(final User user)
            throws UnauthorizedException, NotFoundException
    {
        // If not signed in, throw a 401 error.
        if (user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // Get the Profile entity for the user
        Profile profile = ofy().load().key(Key.create(Profile.class, getUserId(user))).now();
        if (profile == null)
        {
            throw new NotFoundException("Profile doesn't exist.");
        }

        // Get the value of the profile's conferenceKeysToAttend property
        List<String> keyStringsToAttend = profile.getConferenceKeysToAttend();

        // Iterate over keyStringsToAttend, and return a Collection of the
        // Conference entities that the user has registered to attend
        List<Key<Conference>> keysToAttend = new ArrayList<>();
        for (String keyString : keyStringsToAttend)
        {
            keysToAttend.add(Key.<Conference>create(keyString));
        }

        return ofy().load().keys(keysToAttend).values();
    }

    @ApiMethod(
            name = "getAnnouncement",
            path = "announcement",
            httpMethod = HttpMethod.GET)
    public Announcement getAnnouncement()
    {
        MemcacheService memcacheService = MemcacheServiceFactory.getMemcacheService();
        Object message = memcacheService.get(Constants.MEMCACHE_ANNOUNCEMENTS_KEY);
        if (message != null)
        {
            return new Announcement(message.toString());
        }

        return null;
    }

    /**
     * This is an ugly workaround for null userId for Android clients.
     *
     * @param user A User object injected by the cloud endpoints.
     * @return the App Engine userId for the user.
     */
    private static String getUserId(User user)
    {
        String userId = user.getUserId();
        if (userId == null)
        {
            Logger.getLogger("userId is null, so trying to obtain it from the datastore.");

            AppEngineUser appEngineUser = new AppEngineUser(user);
            ofy().save().entity(appEngineUser).now();
            // Begin new session for not using session cache.
            Objectify objectify = ofy().factory().begin();
            AppEngineUser savedUser = objectify.load().key(appEngineUser.getKey()).now();
            userId = savedUser.getUser().getUserId();

            Logger.getLogger("Obtained the userId: " + userId);
        }
        return userId;
    }
}
