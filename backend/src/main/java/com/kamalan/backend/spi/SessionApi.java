package com.kamalan.backend.spi;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.kamalan.backend.Constants;
import com.kamalan.backend.model.AppEngineUser;
import com.kamalan.backend.model.Conference;
import com.kamalan.backend.model.Profile;
import com.kamalan.backend.model.ProfileForm;
import com.kamalan.backend.model.Session;
import com.kamalan.backend.model.SessionForm;
import com.kamalan.backend.model.SessionQueryForm;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.kamalan.backend.service.OfyService.factory;
import static com.kamalan.backend.service.OfyService.ofy;

/**
 * Created by hesam on 3/7/15
 */
@Api(
        name = "sessionApi",
        version = "v1",
        scopes = {Constants.EMAIL_SCOPE},
        clientIds = {Constants.WEB_CLIENT_ID, Constants.ANDROID_CLIENT_ID,
                Constants.API_EXPLORER_CLIENT_ID},
        audiences = {Constants.ANDROID_AUDIENCE},
        description = "Session API for the Conference Central Backend application.")
public class SessionApi
{
    @ApiMethod(name = "createSession", path = "session", httpMethod = ApiMethod.HttpMethod.POST)
    public Session createSession(final User user,
                                 @Named("websafeConferenceKey") final String websafeConferenceKey,
                                 final SessionForm sessionForm)
            throws UnauthorizedException, NotFoundException
    {
        // If the user is not logged in, throw an UnauthorizedException
        if(user == null)
        {
            throw new UnauthorizedException("Authorization required");
        }

        // 404 when there is no Conference with the given conferenceId.
        if(!isWebsafeConferenceKeyValid(websafeConferenceKey))
        {
            throw new NotFoundException("No Conference found with key: " + websafeConferenceKey);
        }

        // Get user id
        final String userId = getUserId(user);

        // Get the key for the user's profile
        Key<Profile> profileKey = Key.create(Profile.class, userId);

        // Allocate a key for the session
        final Key<Session> sessionKey = factory().allocateId(profileKey, Session.class);

        // Get the conference Id from the key
        final long sessionId = sessionKey.getId();

        // Create a new Session Entity, specifying the user's Profile entity
        // as the parent of the session
        Session session = new Session(sessionId, sessionForm);
        session.setWebsafeConferenceKey(websafeConferenceKey);

        // Get the existing Profile entity for the current user if there is one
        // Otherwise create a new Profile entity with default values
        Profile profile = getProfileFromUser(user);

        // Save Session and Profile Entities
        ofy().save().entities(session, profile).now();

        return session;
    }

    /**
     * Queries against the datastore with the given filters and returns the result.
     *
     * Normally this kind of method is supposed to get invoked by a GET HTTP method,
     * but we do it with POST, in order to receive sessionQueryForm Object via the POST body.
     *
     * @param sessionQueryForm A form object representing the query.
     * @return A List of Sessions that match the query.
     */
    @ApiMethod(name = "querySessions", path = "session/querySessions", httpMethod = ApiMethod.HttpMethod.POST)
    public List<Session> querySessions(
            @Named("websafeConferenceKey") final String websafeConferenceKey,
            final SessionQueryForm sessionQueryForm)
    {
        Iterable<Session> sessionIterable = sessionQueryForm.getQuery(websafeConferenceKey);
        List<Session> result = new ArrayList<>(0);
//        List<Key<Profile>> organizersKeyList = new ArrayList<>(0);

        for (Session session : sessionIterable)
        {
//            organizersKeyList.add(Key.create(Profile.class, session.getOrganizerUserId()));
            result.add(session);
        }

        // To avoid separate datastore gets for each Conference, pre-fetch the Profiles.
//        ofy().load().keys(organizersKeyList);
        return result;
    }

    @ApiMethod(name = "deleteSession", path = "session", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deleteSession(final @Named("sessionId") long sessionId)
    {
        ofy().delete().type(Session.class).id(sessionId).now();
    }

    /**
     * Gets the Profile entity for the current user or creates it if it doesn't exist
     *
     * @param user
     * @return user's Profile
     */
    private static Profile getProfileFromUser(User user)
    {
        // First fetch the user's Profile from the datastore.
        Profile profile = ofy().load().key(Key.create(Profile.class, getUserId(user))).now();
        if (profile == null)
        {
            // Create a new Profile if it doesn't exist.
            // Use default displayName and teeShirtSize
            String email = user.getEmail();
            profile =
                    new Profile(getUserId(user), extractDefaultDisplayNameFromEmail(email), email,
                            ProfileForm.TeeShirtSize.NOT_SPECIFIED);
        }

        return profile;
    }

    /**
     * This is an ugly workaround for null userId for Android clients.
     *
     * @param user A User object injected by the cloud endpoints.
     * @return the App Engine userId for the user.
     */
    private static String getUserId(User user)
    {
        String userId = user.getUserId();
        if (userId == null)
        {
            Logger.getLogger("userId is null, so trying to obtain it from the datastore.");

            AppEngineUser appEngineUser = new AppEngineUser(user);
            ofy().save().entity(appEngineUser).now();
            // Begin new session for not using session cache.
            Objectify objectify = ofy().factory().begin();
            AppEngineUser savedUser = objectify.load().key(appEngineUser.getKey()).now();
            userId = savedUser.getUser().getUserId();

            Logger.getLogger("Obtained the userId: " + userId);
        }
        return userId;
    }

    private static boolean isWebsafeConferenceKeyValid(String websafeConferenceKey)
    {
        try
        {
            Key<Conference> conferenceKey = Key.create(websafeConferenceKey);
            Conference conference = ofy().load().key(conferenceKey).now();
            return conference != null;
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
        }

        return false;
    }

    /*
     * Get the display name from the user's email. For example, if the email is
     * lemoncake@example.com, then the display name becomes "lemoncake."
     */
    private static String extractDefaultDisplayNameFromEmail(String email)
    {
        return email == null ? null : email.substring(0, email.indexOf("@"));
    }
}
