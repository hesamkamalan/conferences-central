package com.kamalan.backend;

import com.google.api.server.spi.Constant;

/**
 * Created by Hesam on 28/12/14
 */
public class Constants
{
    public static final String WEB_CLIENT_ID = "1057752539674-qur87f5oa3ah77de0dgnbvh5kqc5oa7t.apps.googleusercontent.com";
    public static final String ANDROID_CLIENT_ID = "1057752539674-m15n2g425mj1un6h12smvvtcd276fcsv.apps.googleusercontent.com";
    public static final String IOS_CLIENT_ID = "";

    public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
    public static final String EMAIL_SCOPE = Constant.API_EMAIL_SCOPE;
    public static final String API_EXPLORER_CLIENT_ID = Constant.API_EXPLORER_CLIENT_ID;

    public static final String MEMCACHE_ANNOUNCEMENTS_KEY = "RECENT_ANNOUNCEMENTS";
}
