package com.kamalan.backend.service;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.kamalan.backend.messaging.RegistrationRecord;
import com.kamalan.backend.model.AppEngineUser;
import com.kamalan.backend.model.Conference;
import com.kamalan.backend.model.Profile;
import com.kamalan.backend.model.Session;

/**
 * Objectify service wrapper so we can statically register our persistence classes
 * More on Objectify here : https://code.google.com/p/objectify-appengine/
 */
public class OfyService
{
    /**
     * This static block ensure the entity registration.
     */
    static {
        factory().register(RegistrationRecord.class);
        factory().register(AppEngineUser.class);
        factory().register(Conference.class);
        factory().register(Session.class);
        factory().register(Profile.class);
    }
    /**
     * Use this static method for getting the Objectify service object in order to make sure the
     * above static block is executed before using Objectify.
     *
     * @return Objectify service object.
     */
    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    /**
     * Use this static method for getting the Objectify service factory.
     *
     * @return ObjectifyFactory.
     */
    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
