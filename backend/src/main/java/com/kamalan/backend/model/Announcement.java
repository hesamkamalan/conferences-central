package com.kamalan.backend.model;

/**
 * Created by Hesam on 10/01/15
 * A simple wrapper for announcement message.
 */
public class Announcement
{
    private String message;

    public Announcement()
    {
    }

    public Announcement(String message)
    {
        this.message = message;
    }

    public String getMessage()
    {
        return message;
    }
}
