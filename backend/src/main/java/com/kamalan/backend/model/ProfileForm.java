package com.kamalan.backend.model;

/**
 * Created by Hesam on 28/12/14
 */
public class ProfileForm
{
    public static enum TeeShirtSize
    {
        NOT_SPECIFIED,
        XS,
        S,
        M,
        L,
        XL,
        XXL,
        XXXL
    }

    private String displayName;
    private String mainEmail;
    private TeeShirtSize teeShirtSize;

    // don't let user to create empty object
    private ProfileForm ()
    {}

    /**
     * Constructor for ProfileForm, solely for unit test.
     *
     * @param displayName A String for displaying the user on this system.
     * @param mainEmail An e-mail address for getting notifications from this system.
     * @param teeShirtSize User's teeShirt size
     */
    public ProfileForm(String displayName, String mainEmail, TeeShirtSize teeShirtSize) {
        this.displayName = displayName;
        this.mainEmail = mainEmail;
        this.teeShirtSize = teeShirtSize;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getMainEmail()
    {
        return mainEmail;
    }

    public TeeShirtSize getTeeShirtSize()
    {
        return teeShirtSize;
    }
}
