package com.kamalan.backend.model;

import com.google.common.base.Preconditions;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Date;

/**
 * Created by hesam on 3/1/15
 */
@Entity
@Cache
public class Session
{
    public enum SessionType { LECTURE, KEYNOTE, WORKSHOP }

    @Id
    private long id;
    private String name;
    private String description;
    @Index
    private String websafeConferenceKey;
    @Index
    private Date startTime;
    private int duration;
    private String topic;
    private String lecturer;
    @Index
    private SessionType sessionType;

    // making default constructor private
    private Session() {
    }

    public Session(final long id, final SessionForm sessionForm) {
        Preconditions.checkNotNull(sessionForm.getName(), "The name is required");
        this.id = id;
        updateWithSessionForm(sessionForm);
    }

    /**
     * Updates the Session with SessionForm.
     * This method is used upon object creation as well as updating existing Sessions.
     *
     * @param sessionForm contains form data sent from the client.
     */
    public void updateWithSessionForm(final SessionForm sessionForm)
    {
        this.name = sessionForm.getName();

        if(sessionForm.getDescription() != null && !sessionForm.getDescription().equals(""))
            this.description = sessionForm.getDescription();
        else
            this.description = "GCE is super interesting topic...";

        if(sessionForm.getTopic() != null && !sessionForm.getTopic().equals(""))
            this.topic = sessionForm.getTopic();
        else
            this.topic = "Google Cloud Endpoint!";

        if(sessionForm.getLecturer() != null && !sessionForm.getLecturer().equals(""))
            this.lecturer = sessionForm.getLecturer();
        else
            this.lecturer = "Hesam";

        if (sessionForm.getStartTime() == null || sessionForm.getStartTime().getTime() <= 0)
            this.startTime = new Date(System.currentTimeMillis());
        else
            this.startTime = sessionForm.getStartTime();

        if(sessionForm.getDuration() > 0)
            this.duration = sessionForm.getDuration();
        else
            this.duration = 120;

        this.sessionType = sessionForm.getSessionType() == null ? Session.SessionType.WORKSHOP : sessionType;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getStartTime() {
        return startTime == null ? null : new Date(startTime.getTime());
    }

    public int getDuration() {
        return duration;
    }

    public String getTopic() {
        return topic;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setWebsafeConferenceKey(String websafeConferenceKey) {
        this.websafeConferenceKey = websafeConferenceKey;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder("Id: " + id + "\n")
            .append("Name: ")
            .append(name)
            .append("\n")
            .append("Description: ")
            .append(description)
            .append("\n")
            .append("Topic: ")
            .append(topic)
            .append("\n")
            .append("Lecturer: ")
            .append(lecturer)
            .append("\n")
            .append("startTime: ")
            .append(startTime)
            .append("\n")
            .append("duration: ")
            .append(duration)
            .append("\n");

        return stringBuilder.toString();
    }
}
