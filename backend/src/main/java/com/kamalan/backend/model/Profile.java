package com.kamalan.backend.model;

import com.google.common.collect.ImmutableList;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.kamalan.backend.model.ProfileForm.TeeShirtSize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hesam on 28/12/14
 */
@Entity
@Cache
public class Profile
{
    @Id
    String userId;
    String displayName;
    String mainEmail;
    TeeShirtSize teeShirtSize;

    // List of conferences the user has registered to attend
    private List<String> conferenceKeysToAttend = new ArrayList<>(0);

    // default constructor is private
    private Profile()
    {
    }

    /**
     * Profile default constructor
     *
     * @param userId
     * @param displayName
     * @param mainEmail
     * @param teeShirtSize
     */
    public Profile(String userId, String displayName, String mainEmail, TeeShirtSize teeShirtSize)
    {
        this.userId = userId;
        this.displayName = displayName;
        this.mainEmail = mainEmail;
        this.teeShirtSize = teeShirtSize;
    }

    /**
     * Update the Profile with the given displayName and teeShirtSize
     *
     * @param displayName
     * @param teeShirtSize
     */
    public void update(String displayName, TeeShirtSize teeShirtSize)
    {
        if (displayName != null)
        {
            this.displayName = displayName;
        }
        if (teeShirtSize != null)
        {
            this.teeShirtSize = teeShirtSize;
        }
    }

    public String getUserId()
    {
        return userId;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getMainEmail()
    {
        return mainEmail;
    }

    public TeeShirtSize getTeeShirtSize()
    {
        return teeShirtSize;
    }

    public List<String> getConferenceKeysToAttend() {
        return ImmutableList.copyOf(conferenceKeysToAttend);
    }

    public void addToConferenceKeysToAttend(String conferenceKey) {
        conferenceKeysToAttend.add(conferenceKey);
    }

    /**
     * Remove the conferenceId from conferenceIdsToAttend.
     *
     * @param conferenceKey a websafe String representation of the Conference Key.
     */
    public void unregisterFromConference(String conferenceKey) {
        if (conferenceKeysToAttend.contains(conferenceKey)) {
            conferenceKeysToAttend.remove(conferenceKey);
        } else {
            throw new IllegalArgumentException("Invalid conferenceKey: " + conferenceKey);
        }
    }
}
