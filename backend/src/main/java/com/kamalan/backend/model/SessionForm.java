package com.kamalan.backend.model;

import java.util.Date;

/**
 * Created by Hesam on 3/1/15
 */
public class SessionForm
{
    private String name;
    private String description;
    private String topic;
    private String lecturer;
    private Date startTime;
    private int duration; // minute
    private Session.SessionType sessionType;

    // making default constructor private
    private SessionForm()
    {
    }

    public SessionForm(String name, String description, String topic, String lecturer,
                       int duration, Session.SessionType sessionType)
    {
        this.name = name;

        if(!description.equals(""))
            this.description = description;
        else
            this.description = "GCE is super interesting topic...";

        if(!topic.equals(""))
            this.topic = topic;
        else
            this.topic = "Google Cloud Endpoint!";

        if(!lecturer.equals(""))
            this.lecturer = lecturer;
        else
            this.lecturer = "Hesam";

        this.startTime = new Date(System.currentTimeMillis());

        if(duration > 0)
            this.duration = duration;
        else
            this.duration = 120;

        this.sessionType = sessionType == null ? Session.SessionType.WORKSHOP : sessionType;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getTopic()
    {
        return topic;
    }

    public String getLecturer() {
        return lecturer;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public int getDuration()
    {
        return duration;
    }

    public Session.SessionType getSessionType() {
        return sessionType;
    }
}
