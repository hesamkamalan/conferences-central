package com.kamalan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appspot.udacity_gae_conference.sessionApi.model.Session;
import com.kamalan.conference.R;
import com.kamalan.utility.Logger;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Hesam on 17/01/15
 */
public class SessionAdapter extends BaseAdapter
{
    private static final String TAG = SessionAdapter.class.getSimpleName();

    private Context mContext;
    private List<Session> mList;

    public SessionAdapter(Context context)
    {
        this.mContext = context;
    }

    public void setSessionList(List<Session> sessionList)
    {
        this.mList = sessionList;
        this.notifyDataSetChanged();
    }

    public void addSession(Session session)
    {
        if (mList == null)
        {
            mList = new ArrayList<>(1);
        }

        this.mList.add(session);
        this.notifyDataSetChanged();
    }

    public void deleteSession(long sessionId)
    {
        int loc = 0;
        for (int i = 0; i < this.mList.size(); i++)
        {
            if (this.mList.get(i).getId() == sessionId) {
                loc = i;
                break;
            }
        }

        this.mList.remove(loc);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount()
    {
        if (this.mList == null)
        {
            return 0;
        }
        else
        {
            return this.mList.size();
        }
    }

    @Override
    public Session getItem(int position)
    {
        if (this.mList == null)
        {
            return null;
        }
        else
        {
            return this.mList.get(position);
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_session, null);

            holder = new ViewHolder();
            holder.tvTopics = (TextView) convertView.findViewById(R.id.tvTopics);
            holder.tvStartDate = (TextView) convertView.findViewById(R.id.tvStartDate);
            holder.tvDuration = (TextView) convertView.findViewById(R.id.tvDuration);
            holder.tvLecturer = (TextView) convertView.findViewById(R.id.tvLecturer);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvTopics.setText(this.mList.get(position).getTopic());
        holder.tvLecturer.setText("Lecturer: " + this.mList.get(position).getLecturer());
        holder.tvDescription.setText("Description: " + this.mList.get(position).getDescription());

        String startDate = this.mList.get(position).getStartTime().toString();
        holder.tvStartDate.setText("Date/Time: " + getFormattedDate(startDate));
        holder.tvDuration.setText("Duration: " + this.mList.get(position).getDuration() + "m");

        return convertView;
    }

    static class ViewHolder
    {
        TextView tvTopics;
        TextView tvStartDate;
        TextView tvDuration;
        TextView tvLecturer;
        TextView tvDescription;
    }

    private String getFormattedDate(String strDate)
    {
        try
        {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            strDate = strDate.replaceAll("Z$", "+0000");
            Date date = sdf1.parse(strDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yyyy - HH:mm");
            return sdf2.format(date);
        }
        catch (ParseException | NullPointerException | IllegalArgumentException e)
        {
            e.printStackTrace();
            Logger.e(TAG, "Exception on parsing data: " + e.getMessage());
        }

        return "";
    }

}
