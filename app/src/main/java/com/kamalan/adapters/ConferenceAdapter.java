package com.kamalan.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.kamalan.conference.R;
import com.kamalan.utility.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Hesam on 17/01/15
 */
public class ConferenceAdapter extends BaseAdapter
{
    public interface IOnConfRegistrationListener {
        public void onConferenceRegistration(String webKey);
        public void onConferenceUnregistration(String webKey);
    }

    private static final String TAG = ConferenceAdapter.class.getSimpleName();

    private Context mContext;
    private List<Conference> mList;
    private IOnConfRegistrationListener mRegistrationListener;
    private boolean isUserRegisteredConference = false;

    public ConferenceAdapter(Context context, IOnConfRegistrationListener registrationListener)
    {
        this.mContext = context;
        this.mRegistrationListener = registrationListener;
    }

    public void setConferenceList(List<Conference> conferenceList, boolean userRegisteredConference)
    {
        this.isUserRegisteredConference = userRegisteredConference;

        this.mList = conferenceList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount()
    {
        if (this.mList == null)
        {
            return 0;
        }
        else
        {
            return this.mList.size();
        }
    }

    @Override
    public Conference getItem(int position)
    {
        if (this.mList == null)
        {
            return null;
        }
        else
        {
            return this.mList.get(position);
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final ViewHolder holder;

        if (convertView == null)
        {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_conference, null);

            holder = new ViewHolder();
            holder.tvConfDate = (TextView) convertView.findViewById(R.id.tvConfDate);
            holder.tvConfOrganizer = (TextView) convertView.findViewById(R.id.tvConfOrganizer);
            holder.tvConfName = (TextView) convertView.findViewById(R.id.tvConfName);
            holder.tvConfDescription = (TextView) convertView.findViewById(R.id.tvConfDescription);
            holder.tvConfTopics = (TextView) convertView.findViewById(R.id.tvConfTopics);
            holder.tvConfSeats = (TextView) convertView.findViewById(R.id.tvConfSeats);
            holder.ibConfRegister = (ImageButton) convertView.findViewById(R.id.ibConfRegister);
            holder.ibConfRegister.setFocusable(false);
            holder.ibConfRegister.setFocusableInTouchMode(false);
            holder.ibConfRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int tag = (int) v.getTag();
                    if(isUserRegisteredConference) {
                        mRegistrationListener.onConferenceUnregistration(mList.get(tag).getWebsafeKey());
                    } else {
                        mRegistrationListener.onConferenceRegistration(mList.get(tag).getWebsafeKey());
                    }
                }
            });

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        // Assign delete icon if "To Attend" is selected, show add icon otherwise
        if(isUserRegisteredConference)
        {
            holder.ibConfRegister.setImageResource(android.R.drawable.ic_delete);
        }
        else
        {
            holder.ibConfRegister.setImageResource(android.R.drawable.ic_input_add);
        }

        // Set position as tag to imageButton
        holder.ibConfRegister.setTag(position);

        // Start Date, End Date, City, Organizer
        StringBuilder dateBuilder = new StringBuilder();
        dateBuilder.append(getFormattedDate(mList.get(position).getStartDate().toString()));
        dateBuilder.append(" - ");
        dateBuilder.append(getFormattedDate(mList.get(position).getEndDate().toString()));
        dateBuilder.append(" in ");
        dateBuilder.append(mList.get(position).getCity());
        holder.tvConfDate.setText(dateBuilder.toString());
        holder.tvConfOrganizer.setText("by " + mList.get(position).getOrganizerDisplayName());

        // Name
        holder.tvConfName.setText(mList.get(position).getName());

        // Description
        holder.tvConfDescription.setText(mList.get(position).getDescription());

        // Topics
        List<String> topics = mList.get(position).getTopics();
        String allTopics = "";
        for (String topic : topics)
            allTopics += (topic + ", ");
        allTopics = allTopics.substring(0, allTopics.length() - 2);
        holder.tvConfTopics.setText(allTopics);

        // Seats Ave., MaxAttendees
        int avlSeats = mList.get(position).getSeatsAvailable();
        int maxSeats = mList.get(position).getMaxAttendees();
        String strSeats = avlSeats + "/" + maxSeats + " seats available";
        holder.tvConfSeats.setText(strSeats);

        // Set seats available and assign it red color if is less than 10
        if (avlSeats <= 10)
        {
            holder.tvConfSeats.setTextColor(Color.RED);
        }
        else
        {
            holder.tvConfSeats.setTextColor(Color.BLACK);
        }

        // Set start/end data based on conference status
        int confState = getConferenceState(mList.get(position));
        switch (confState)
        {
            case 1: // finished
                holder.tvConfName.setTextColor(Color.GRAY);
                holder.tvConfSeats.setTextColor(Color.GRAY);
                break;
            case 2: // less than 3 days
                holder.tvConfName.setTextColor(Color.RED);
                break;
            case 3: // less than 7 days
                holder.tvConfName.setTextColor(Color.MAGENTA);
                break;
            case 4: // more than 7 days
                holder.tvConfName.setTextColor(Color.BLACK);
                break;
            case 5: // in progress
                holder.tvConfName.setTextColor(Color.GREEN);
                break;
            default:
                holder.tvConfName.setTextColor(Color.BLACK);
        }

        return convertView;
    }

    static class ViewHolder
    {
        TextView tvConfDate;
        TextView tvConfOrganizer;
        TextView tvConfName;
        TextView tvConfDescription;
        TextView tvConfTopics;
        TextView tvConfSeats;
        ImageButton ibConfRegister;
    }

    /**
     * Returns conference status.
     *
     * @param conference
     * @return Status of conference as follows:
     * 1: Conference finished
     * 2: Less than 3 days to commence date
     * 3: Less than 7 days to commence date
     * 4: More than 7 days to commence date
     * 5: Conference is in progress
     */
    private int getConferenceState(Conference conference)
    {
        try
        {
            Date currentTime = new Date();

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String strDate1 = conference.getStartDate().toString().replaceAll("Z$", "+0000");
            Date startDate = sdf1.parse(strDate1);
            String strDate2 = conference.getEndDate().toString().replaceAll("Z$", "+0000");
            Date endDate = sdf1.parse(strDate2);

            if (currentTime.getTime() >= endDate.getTime())
            {
                return 1;
            }
            else if(currentTime.getTime() > startDate.getTime()  &&
                    currentTime.getTime() < endDate.getTime())
            {
                return 5;
            }
            else if (startDate.getTime() - currentTime.getTime() <=
                     TimeUnit.MILLISECONDS.convert(3, TimeUnit.DAYS))
            {
                return 2;
            }
            else if (startDate.getTime() - currentTime.getTime() <=
                     TimeUnit.MILLISECONDS.convert(7, TimeUnit.DAYS))
            {
                return 3;
            }
            else if (currentTime.getTime() < startDate.getTime())
            {
                return 4;
            }
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            Logger.e(TAG, "Exception on parsing data: " + e.getMessage());
        }

        return 4;
    }

    private String getFormattedDate(String strDate)
    {
        try
        {
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            strDate = strDate.replaceAll("Z$", "+0000");
            Date date = sdf1.parse(strDate);

            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM");
            return sdf2.format(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            Logger.e(TAG, "Exception on parsing data: " + e.getMessage());
        }

        return "";
    }

}
