package com.kamalan.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.appspot.udacity_gae_conference.conferenceApi.model.WrappedBoolean;
import com.kamalan.adapters.ConferenceAdapter;
import com.kamalan.conference.R;
import com.kamalan.endpoints.ConferencesEndpoint;
import com.kamalan.endpoints.ConferencesEndpoint.ConferenceFilter;
import com.kamalan.endpoints.RegistrationEndpoint;
import com.kamalan.utility.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A fragment representing a list of Conferences.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class ConferencesFragment extends Fragment implements AbsListView.OnItemClickListener,
        ConferencesEndpoint.IConferencesEndpointListener,
        ConferenceAdapter.IOnConfRegistrationListener,
        RegistrationEndpoint.IOnRegistrationEndPointListener
{
    private static final String TAG = ConferencesFragment.class.getSimpleName();

    // Listener to react to conference item click
    private OnFragmentInteractionListener mListener;

    // The fragment's ListView/GridView.
    private AbsListView mListView;
    private ConferenceAdapter mListAdapter;

    // false=Asc, true=Dsc
    private boolean sortDirection = false;
    private MenuItem sortMenu;

    // Conference list
    private List<Conference> mConferenceList;

    // Set to True when user select "To Attend" item from overflow menu
    private boolean isToAttendItemSelected = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ConferencesFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_conference, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // create adapter
        mListAdapter = new ConferenceAdapter(getActivity(), this);
        this.mListView.setAdapter(mListAdapter);
    }

    @Override
    public void onStart()
    {
        super.onStart();

        // Get list of conferences
        new ConferencesEndpoint(getActivity(), ConferenceFilter.ALL, this).execute();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(
                    activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (null != mListener)
        {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Conference conference = (Conference) parent.getAdapter().getItem(position);
            mListener.onConferenceSelected(conference);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.frag_conf_menu, menu);

        sortMenu = menu.findItem(R.id.action_sort_asc);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        // Sort requested
        if (id == R.id.action_sort_asc)
        {
            Collections.reverse(this.mConferenceList);
            mListAdapter.setConferenceList(this.mConferenceList, isToAttendItemSelected);

            this.sortDirection = !this.sortDirection;
            getActivity().invalidateOptionsMenu();

            return true;
        }

        // reset first, it should be defined after R.id.action_sort_asc
        this.isToAttendItemSelected = false;

        // Call MyConferencesEndpoint
        if (id == R.id.action_all_conferences)
        {
            new ConferencesEndpoint(getActivity(), ConferenceFilter.ALL, this).execute();
            return true;
        }

        // Call MyConferencesEndpoint
        if (id == R.id.action_i_created)
        {
            new ConferencesEndpoint(getActivity(), ConferenceFilter.I_CREATED, this).execute();
            return true;
        }

        // Call ConferencesToAttendEndpoint
        if (id == R.id.action_to_attend)
        {
            getToAttendConferenceList();
            return true;
        }

        // Call ConferencesToAttendEndpoint
        if (id == R.id.action_filter_city)
        {
            // custom dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.dialog_search_city);
            dialog.setTitle("City to be searched:");

            final EditText etCityName = (EditText) dialog.findViewById(R.id.etCityName);
            Button btnSearch = (Button) dialog.findViewById(R.id.btnSearch);
            btnSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String city = etCityName.getText().toString().trim();
                    if(TextUtils.isEmpty(city)) {
                        displayMessage("Please enter a city name!");
                        return;
                    }

                    ConferencesEndpoint endpoint = new ConferencesEndpoint(getActivity(),
                            ConferenceFilter.FILTER_BY_CITY, ConferencesFragment.this);
                    endpoint.setCityName(city);
                    endpoint.execute();

                    dialog.dismiss();
                }
            });
            dialog.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        updateSortIcon();

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onConferenceListReceived(List<Conference> conferenceList)
    {
        if (this.getView() == null)
        {
            return;
        }

        if (conferenceList == null)
        {
            Logger.e(TAG, "Conference list is null :(");
            conferenceList = new ArrayList<>();

            getView().findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
        }
        else
        {
            getView().findViewById(android.R.id.empty).setVisibility(View.GONE);
        }

        for (Conference conference : conferenceList)
        {
            Logger.d(TAG, conference.toString());
        }

        // keep copy of conferenceList for sorting and update adapter
        this.mConferenceList = conferenceList;
        mListAdapter.setConferenceList(conferenceList, isToAttendItemSelected);
    }

    @Override
    public void onConferenceRegistration(String webKey) {
        new RegistrationEndpoint(getActivity(), webKey, RegistrationEndpoint.RegistrationStatus.REGISTER, this).execute();
    }

    @Override
    public void onConferenceUnregistration(String webKey) {
        new RegistrationEndpoint(getActivity(), webKey, RegistrationEndpoint.RegistrationStatus.UNREGISTER, this).execute();
    }

    @Override
    public void onUserRegisteredForConference(WrappedBoolean status) {
        if (status == null)
        {
            displayMessage("Oops, seems you've registered before");
            return;
        }

        if (!status.getResult())
            displayMessage(status.getReason());
        else
            displayMessage("Registration Successful!");
    }

    @Override
    public void onUserUnregisteredFromConference(WrappedBoolean status) {
        if (status == null)
        {
            displayMessage("Oops, seems you've unregistered before");
            return;
        }

        if (!status.getResult())
            displayMessage(status.getReason());
        else
            getToAttendConferenceList();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated to
     * the activity and potentially other fragments contained in that activity.
     */
    public interface OnFragmentInteractionListener
    {
        public void onConferenceSelected(Conference conference);
    }

    private void displayMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void updateSortIcon()
    {
        if (sortMenu == null)
            return;

        if (sortDirection)
            sortMenu.setIcon(R.drawable.ic_arrow_up);
        else
            sortMenu.setIcon(R.drawable.ic_arrow_down);
    }

    private void getToAttendConferenceList()
    {
        this.isToAttendItemSelected = true;
        new ConferencesEndpoint(getActivity(), ConferenceFilter.TO_ATTEND, this).execute();
    }
}
