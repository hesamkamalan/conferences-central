package com.kamalan.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.*;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.sessionApi.model.Session;
import com.kamalan.adapters.SessionAdapter;
import com.kamalan.conference.R;
import com.kamalan.endpoints.CreateSessionEndpoint;
import com.kamalan.endpoints.SessionEndpoint;
import com.kamalan.utility.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A fragment representing a list of Sessions.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 */
public class SessionFragment extends Fragment implements SessionEndpoint.ISessionsEndpointListener,
        CreateSessionEndpoint.ICreateSessionEndpointListener
{
    private static final String TAG = SessionFragment.class.getSimpleName();

    // the fragment initialization parameters
    private static final String WEBSAFE_KEY = "websafeKey";

    private String conferenceWebsafeKey;

    private AbsListView mListView;
    private SessionAdapter mListAdapter;


    public static SessionFragment newInstance(String conferenceWebsafeKey) {
        SessionFragment fragment = new SessionFragment();
        Bundle args = new Bundle();
        args.putString(WEBSAFE_KEY, conferenceWebsafeKey);
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SessionFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            conferenceWebsafeKey = getArguments().getString(WEBSAFE_KEY);
            Logger.d(TAG, "Conf websafeKey: " + conferenceWebsafeKey);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_session, container, false);

        // Set the adapter
        this.mListView = (AbsListView) view.findViewById(android.R.id.list);
        this.mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Session session = SessionFragment.this.mListAdapter.getItem(position);
                showDeleteConfirmDialog(session);

                return true;
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // create adapter
        this.mListAdapter = new SessionAdapter(getActivity());
        this.mListView.setAdapter(mListAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        getListOfAllSessions();
    }

    @Override
    public void onSessionListReceived(List<Session> sessionList) {
        if (this.getView() == null) {
            return;
        }

        if (sessionList == null) {
            Logger.e(TAG, "Session list is null :(");
            getView().findViewById(android.R.id.empty).setVisibility(View.VISIBLE);

            return;
        } else {
            getView().findViewById(android.R.id.empty).setVisibility(View.GONE);
        }

        for (Session session : sessionList) {
            Logger.d(TAG, session.toString());
        }

        mListAdapter.setSessionList(sessionList);
    }

    @Override
    public void onSessionDeleted(long sessionId) {
        this.mListAdapter.deleteSession(sessionId);
    }

    @Override
    public void onSessionCreated(Session session) {
        if (this.getView() == null)
        {
            return;
        }

        if (session == null)
        {
            Logger.e(TAG, "Error, Session did not create :(");
            displayMessage("Error in creating new Session :( Please try later.");

            return;
        }

        if(getView().findViewById(android.R.id.empty).getVisibility() != View.GONE)
        {
            getView().findViewById(android.R.id.empty).setVisibility(View.GONE);
        }

        Logger.d(TAG, "Session created: " + session.toString());
        mListAdapter.addSession(session);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.frag_session_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_session_create)
        {
            // Create and show the dialog.
            DialogFragment dialog = new SessionCreateDialogFragment(this, conferenceWebsafeKey);
            showDialog(dialog);

            return true;
        }

        if (id == R.id.action_session_all) {
            getListOfAllSessions();

            return true;
        }

        if (id == R.id.action_session_filter) {
            getActivity().invalidateOptionsMenu();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getListOfAllSessions() {
        // Get list of conferences
        new SessionEndpoint(getActivity(), this.conferenceWebsafeKey, SessionEndpoint.SessionFilter.ALL, this).execute();
    }

    private void displayMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void showDialog(DialogFragment dialogFragment)
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null)
        {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        if (dialogFragment != null) {
            dialogFragment.show(ft, "dialog");
        }
    }

    private void showDeleteConfirmDialog(final Session session)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_delete_session_title)
                .setMessage(R.string.dialog_delete_session_message)
                .setPositiveButton(R.string.dialog_delete_session_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        new SessionEndpoint(SessionFragment.this.getActivity(),
                                session.getId(),
                                SessionEndpoint.SessionFilter.DELETE,
                                SessionFragment.this).execute();
                    }
                })
                .setNegativeButton(R.string.dialog_delete_session_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });

        // Create the AlertDialog object
        builder.show();
    }
}
