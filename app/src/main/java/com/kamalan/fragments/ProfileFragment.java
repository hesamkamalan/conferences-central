package com.kamalan.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.conferenceApi.model.Profile;
import com.appspot.udacity_gae_conference.conferenceApi.model.ProfileForm;
import com.kamalan.conference.R;
import com.kamalan.endpoints.GetProfileEndpoint;
import com.kamalan.endpoints.UpdateProfileEndpoint;
import com.kamalan.utility.Logger;

/**
 * Created by Hesam on 24/01/15
 */
public class ProfileFragment extends Fragment implements GetProfileEndpoint.IGetProfileEndpoint
{
    private static final String TAG = ProfileFragment.class.getSimpleName();

    private EditText mEtUserName;
    private Spinner mSpUserSize;

    public ProfileFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Hide options menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mEtUserName = (EditText) view.findViewById(R.id.etDisplayName);
        mSpUserSize = (Spinner) view.findViewById(R.id.spUserShirtSize);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        new GetProfileEndpoint(getActivity(), this).execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_action_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_save)
        {
            String userName = mEtUserName.getText().toString().trim();
            if (userName.equals(""))
            {
                displayMessage("Name shouldn't be empty.");
                return true;
            }

            ProfileForm form = new ProfileForm();
            form.setDisplayName(userName);
            form.setTeeShirtSize(mSpUserSize.getSelectedItem().toString());

            new UpdateProfileEndpoint(getActivity(), form, ProfileFragment.this).execute();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProfileReceived(Profile profile)
    {
        if (profile != null)
        {
            Logger.d(TAG, profile.toString());

            displayData(profile);
        }
        else
        {
            Logger.i(TAG, "Profile is null :(");
            displayMessage(getString(R.string.ep_no_data_1));
        }
    }

    private void displayMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private void displayData(Profile profile)
    {
        mEtUserName.setText(profile.getDisplayName());

        switch (profile.getTeeShirtSize())
        {
            case "XS": mSpUserSize.setSelection(0); break;
            case "S": mSpUserSize.setSelection(1); break;
            case "M": mSpUserSize.setSelection(2); break;
            case "L": mSpUserSize.setSelection(3); break;
            case "XL": mSpUserSize.setSelection(4); break;
            case "XXL": mSpUserSize.setSelection(5); break;
            case "XXXL": mSpUserSize.setSelection(6); break;
            default:
                mSpUserSize.setSelection(3);
        }
    }
}
