package com.kamalan.fragments;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.sessionApi.model.SessionForm;
import com.google.api.client.util.DateTime;
import com.kamalan.conference.R;
import com.kamalan.endpoints.CreateSessionEndpoint;
import com.kamalan.utility.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by hesam on 3/14/15
 */
public class SessionCreateDialogFragment extends DialogFragment implements View.OnClickListener
{
    private static final String TAG = SessionCreateDialogFragment.class.getSimpleName();

    private final SessionFragment sessionFragment;
    private final String conferenceWebSafeKey;

    private EditText etName;
    private EditText etTopic;
    private EditText etLecturer;
    private EditText etDescription;
    private EditText etStartDate;
    private EditText etStartTime;
    private Spinner spSessionType;
    private Spinner spDuration;
    private ImageButton ibStartDate;
    private ImageButton ibStartTime;
    private Button btnCancel;
    private Button btnSubmit;

    private Date startDate;
    private int hour;
    private int minute;

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    // To prevent this part runs twice
                    if (!view.isShown())
                    {
                        return;
                    }

                    SessionCreateDialogFragment.this.hour = hourOfDay;
                    SessionCreateDialogFragment.this.minute = minute;

                    String selectedTime;
                    if (minute > 9)
                        selectedTime = hourOfDay + ":" + minute;
                    else
                        selectedTime = hourOfDay + ":0" + minute;
                    SessionCreateDialogFragment.this.etStartTime.setText(selectedTime);
                    Logger.d(TAG, "Time: " + selectedTime);
                }
            };

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    // To prevent this part runs twice
                    if (!view.isShown())
                    {
                        return;
                    }

                    Date selectedDate =
                            new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
                    // dialog returns: Mon Jan 26 00:00:00 GMT+08:00 2015
                    Logger.d(TAG, "Date: " + selectedDate);

                    // Check is correct date
                    Date currentDate = new Date();
                    if (selectedDate.getTime() < currentDate.getTime())
                    {
                        displayMessage("You cannot chose past date.");
                        return;
                    }

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(selectedDate);

                    startDate = selectedDate;
                    etStartDate.setText(new SimpleDateFormat("dd MMM yyyy").format(cal.getTime()));
                }
            };

    public SessionCreateDialogFragment()
    {
        this(null, "");
    }

    public SessionCreateDialogFragment(SessionFragment sessionFragment, String conferenceWebSafeKey)
    {
        this.sessionFragment = sessionFragment;
        this.conferenceWebSafeKey = conferenceWebSafeKey;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_create_session, container, false);

        etName = (EditText) view.findViewById(R.id.etSessionName);
        etTopic = (EditText) view.findViewById(R.id.etSessionTopic);
        etLecturer = (EditText) view.findViewById(R.id.etSessionLecturer);
        etDescription = (EditText) view.findViewById(R.id.etSessionDescription);
        etStartDate = (EditText) view.findViewById(R.id.etSessionStartDate);
        etStartTime = (EditText) view.findViewById(R.id.etSessionStartTime);
        spSessionType = (Spinner) view.findViewById(R.id.spSessionType);
        spDuration = (Spinner) view.findViewById(R.id.spSessionDuration);
        ibStartDate = (ImageButton) view.findViewById(R.id.ibSessionStartDate);
        ibStartTime = (ImageButton) view.findViewById(R.id.ibSessionStartTime);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        ibStartDate.setOnClickListener(this);
        ibStartTime.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().setTitle("Create Session");
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ibSessionStartDate:
                Calendar c = new GregorianCalendar();
                int curDay = c.get(Calendar.DAY_OF_MONTH);
                int curMonth = c.get(Calendar.MONTH);
                int curYear = c.get(Calendar.YEAR);

                DatePickerDialog dialog1 =
                        new DatePickerDialog(SessionCreateDialogFragment.this.getActivity(),
                                mDateSetListener, curYear, curMonth, curDay);
                dialog1.show();
                return;

            case R.id.ibSessionStartTime:
                Calendar cl = Calendar.getInstance();
                int hour = cl.get(Calendar.HOUR_OF_DAY);
                int minute = cl.get(Calendar.MINUTE);

                TimePickerDialog dialog2 =
                        new TimePickerDialog(SessionCreateDialogFragment.this.getActivity(),
                                mTimeSetListener, hour, minute, true);
                dialog2.show();
                return;

            case R.id.btnCancel:
                getDialog().dismiss();
                return;

            case R.id.btnSubmit:
                if (checkForm())
                {
                    SessionForm form = getSessionForm();
                    new CreateSessionEndpoint(getActivity(), conferenceWebSafeKey, form, sessionFragment).execute();

                    getDialog().dismiss();
                }
                return;
        }
    }

    private void displayMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    private boolean checkForm()
    {
        if (TextUtils.isEmpty(etName.getText()))
        {
            displayMessage("Name field is empty.");
            return false;
        }

        return true;
    }

    private SessionForm getSessionForm()
    {
        SessionForm form = new SessionForm();
        form.setName(etName.getText().toString());
        form.setTopic(etTopic.getText().toString());
        form.setLecturer(etLecturer.getText().toString());
        form.setDescription(etDescription.getText().toString());

        String sessionType = spSessionType.getSelectedItem().toString().toUpperCase(Locale.US);
        form.setSessionType(sessionType);

        int duration = spDuration.getSelectedItemPosition();
        switch (duration)
        {
            case 0: form.setDuration(60); break;  // 1 hour
            case 1: form.setDuration(120); break; // 2 hours
            case 2: form.setDuration(180); break; // 3 hours
            default:form.setDuration(240); break; // 4 hours
        }

        if (startDate != null) {
            long selectedDateInMillis = startDate.getTime();
            long selectedTimeInMillis = (SessionCreateDialogFragment.this.hour * 60 +
                    SessionCreateDialogFragment.this.minute) * 60 * 1000;
            form.setStartTime(new DateTime(selectedDateInMillis + selectedTimeInMillis));
        }
        else
        {
            form.setStartTime(new DateTime(System.currentTimeMillis()));
        }

        return form;
    }
}
