package com.kamalan.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.appspot.udacity_gae_conference.conferenceApi.model.ConferenceForm;
import com.google.api.client.util.DateTime;
import com.kamalan.conference.R;
import com.kamalan.endpoints.CreateConferenceEndpoint;
import com.kamalan.utility.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Hesam on 24/01/15
 */
public class CreateConferenceFragment extends Fragment implements CreateConferenceEndpoint.ICreateConferenceEndpointListener
{
    private static final String TAG = CreateConferenceFragment.class.getSimpleName();

    private EditText mEtConfName;
    private EditText mEtCityName;
    private EditText mEtConfDescription;
    private Spinner mSpTopics;
    private EditText mEtStartDate;
    private ImageButton mIbStartDate;
    private EditText mEtEndDate;
    private ImageButton mIbEndDate;
    private EditText mEtMaxAttendees;

    private boolean isStartDateClicked;
    private Date startDate;
    private Date endDate;

    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener()
            {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                {
                    // To prevent this part runs twice
                    if (!view.isShown())
                    {
                        return;
                    }

                    Date selectedDate =
                            new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
                    // dialog returns: Mon Jan 26 00:00:00 GMT+08:00 2015
                    Logger.i(TAG, "Date:" + selectedDate);

                    // Check is correct date
                    Date currentDate = new Date();
                    if (selectedDate.getTime() < currentDate.getTime())
                    {
                        displayMessage("You cannot chose past date.");
                        return;
                    }

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(selectedDate);

                    if (isStartDateClicked)
                    {
                        if (endDate != null  &&  selectedDate.getTime() > endDate.getTime())
                        {
                            mEtStartDate.setText("");
                            displayMessage("Start date cannot be after End date");
                            return;
                        }

                        startDate = selectedDate;
                        mEtStartDate
                                .setText(new SimpleDateFormat("dd MMM yyyy").format(cal.getTime()));
                    }
                    else
                    {
                        if (startDate != null  &&  selectedDate.getTime() < startDate.getTime())
                        {
                            mEtEndDate.setText("");
                            displayMessage("End date cannot be before Start date");
                            return;
                        }

                        endDate = selectedDate;
                        mEtEndDate
                                .setText(new SimpleDateFormat("dd MMM yyyy").format(cal.getTime()));
                    }
                }
            };

    public CreateConferenceFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Show options menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_create_conference, container, false);

        mEtConfName = (EditText) view.findViewById(R.id.etConfName);
        mEtCityName = (EditText) view.findViewById(R.id.etCityName);
        mEtConfDescription = (EditText) view.findViewById(R.id.etConfDescription);
        mSpTopics = (Spinner) view.findViewById(R.id.spTopics);
        mEtStartDate = (EditText) view.findViewById(R.id.etStartDate);
        mEtEndDate = (EditText) view.findViewById(R.id.etEndDate);
        mEtMaxAttendees = (EditText) view.findViewById(R.id.etMaxAttendees);
        mIbStartDate = (ImageButton) view.findViewById(R.id.ibStartDate);
        mIbEndDate = (ImageButton) view.findViewById(R.id.ibEndDate);

        // Prevent keyboard popup
        mEtStartDate.setInputType(EditorInfo.TYPE_NULL);
        mEtEndDate.setInputType(EditorInfo.TYPE_NULL);

        mIbStartDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                isStartDateClicked = true;

                Calendar c = new GregorianCalendar();
                int curDay = c.get(Calendar.DAY_OF_MONTH);
                int curMonth = c.get(Calendar.MONTH);
                int curYear = c.get(Calendar.YEAR);

                DatePickerDialog dialog =
                        new DatePickerDialog(CreateConferenceFragment.this.getActivity(),
                                mDateSetListener, curYear, curMonth, curDay);
                dialog.show();
            }
        });

        mIbEndDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                isStartDateClicked = false;

                Calendar c = new GregorianCalendar();
                int curDay = c.get(Calendar.DAY_OF_MONTH);
                int curMonth = c.get(Calendar.MONTH);
                int curYear = c.get(Calendar.YEAR);

                DatePickerDialog dialog =
                        new DatePickerDialog(CreateConferenceFragment.this.getActivity(),
                                mDateSetListener, curYear, curMonth, curDay);
                dialog.show();
            }
        });

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_action_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_save)
        {
            ConferenceForm form = createConference();
            if (form != null)
            {
                new CreateConferenceEndpoint(getActivity(), form, this).execute();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ConferenceForm createConference()
    {
        String _confName = mEtConfName.getText().toString().trim();
        String _cityName = mEtCityName.getText().toString().trim();
        String _description = mEtConfDescription.getText().toString().trim();
        String _maxAttendees = mEtMaxAttendees.getText().toString().trim();

        if (TextUtils.isEmpty(_confName))
        {
            displayMessage("Conference name must not be empty!");
            return null;
        }

        if (startDate == null)
        {
            displayMessage("Conference must have start date");
            return null;
        }

        if (endDate == null)
        {
            displayMessage("Conference must have end date");
            return null;
        }

        int maxAttendees = 10;
        try
        {
            maxAttendees = Integer.parseInt(_maxAttendees);
        }
        catch (NumberFormatException e)
        {
            Logger.e(TAG, e.toString());
        }

        List<String> _topics = new ArrayList<>();
        _topics.add(mSpTopics.getSelectedItem().toString());

        ConferenceForm form = new ConferenceForm();
        form.setName(_confName);
        form.setCity(_cityName);
        form.setDescription(_description);
        form.setTopics(_topics);
        form.setStartDate(new DateTime(startDate));
        form.setEndDate(new DateTime(endDate));
        form.setMaxAttendees(maxAttendees);

        return form;
    }

    private void displayMessage(String message)
    {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConferenceCreated(Conference conference)
    {
        if (conference != null)
        {
            Logger.d(TAG, conference.toString());

            displayMessage("Conference created successfully!");
        }
        else
        {
            Logger.i(TAG, "Conference is null :(");
            displayMessage(getString(R.string.ep_no_data_1));
        }
    }
}