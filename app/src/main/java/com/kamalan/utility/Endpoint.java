package com.kamalan.utility;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

/**
 * Created by Hesam on 17/01/15
 */
public final class Endpoint
{
    /**
     * Application client Id
     */
    public static final String CLIENT_ID = "udacity-gae-conference";

    /**
     * Your WEB CLIENT ID from the API Access screen of the Developer Console page.
     */
    public static final String WEB_CLIENT_ID = "1057752539674-qur87f5oa3ah77de0dgnbvh5kqc5oa7t.apps.googleusercontent.com";

    /**
     * The audience is defined by the web client id, not the Android client id.
     */
    public static final String AUDIENCE = "server:client_id:" + WEB_CLIENT_ID;

    /**
     * Class instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new AndroidJsonFactory();

    /**
     * Class instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();

}
