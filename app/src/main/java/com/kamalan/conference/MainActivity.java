package com.kamalan.conference;

import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.appspot.udacity_gae_conference.conferenceApi.model.Announcement;
import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.fragments.ConferencesFragment;
import com.kamalan.fragments.CreateConferenceFragment;
import com.kamalan.endpoints.AnnouncementEndpoint;
import com.kamalan.fragments.ProfileFragment;
import com.kamalan.fragments.SessionFragment;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;


public class MainActivity extends Activity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        ConferencesFragment.OnFragmentInteractionListener,
        AnnouncementEndpoint.IAnnouncementEndpoint
{
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_ACCOUNT_PICKER = 1;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    /**
     * AsyncTask, responsible to load announcement from backend
     */
    private AnnouncementEndpoint mAnnouncementEndpoint;

    /**
     * Selected conference
     */
    private Conference mConference;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment
                .setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        // register for receiving push notification
        // https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/GcmEndpoints

        // Get Google account credentials
        GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(this,
                "server:udacity-gae-conference:1-web-app.apps.googleusercontent.com");
        String accountName = Storage.getAccountName(this);
        credential.setSelectedAccountName(accountName);

        if (credential.getSelectedAccountName() != null)
        {
            Logger.d(TAG, "Logged in with: " + credential.getSelectedAccountName());

            // Already signed in, show ConferencesFragment
            replaceFragment(0);
        }
        else
        {
            // Not signed in, show login window or request an account.
            startActivityForResult(credential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        if (mAnnouncementEndpoint == null)
        {
            mAnnouncementEndpoint = new AnnouncementEndpoint(this, this);
            mAnnouncementEndpoint.execute();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        replaceFragment(position);
        onSectionAttached(position);
        restoreActionBar();
    }

    public void onSectionAttached(int number)
    {
        switch (number)
        {
            case 0:
                mTitle = getString(R.string.sliding_menu_section_1);
                break;
            case 1:
                mTitle = getString(R.string.sliding_menu_section_2);
                break;
            case 2:
                mTitle = getString(R.string.sliding_menu_section_3);
                break;
        }
    }

    public void restoreActionBar()
    {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return mNavigationDrawerFragment.onOptionsItemSelected(item) ||
               super.onOptionsItemSelected(item);
    }

    @Override
    public void onConferenceSelected(Conference conference)
    {
        this.mConference = conference;
        this.replaceFragment(3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_ACCOUNT_PICKER:
                if (data != null && data.getExtras() != null)
                {
                    String accountName = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null)
                    {
                        // User is authorized.
                        Storage.setAccountName(MainActivity.this, accountName);
                        Logger.d(TAG, "Acc name: " + accountName);

                        // Show ConferencesFragment
                        replaceFragment(0);
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        if (mNavigationDrawerFragment.isDrawerOpen())
        {
            mNavigationDrawerFragment.closeDrawer();
            return;
        }

        final Fragment fragment = this.getFragmentManager().findFragmentById(R.id.container);
        if (fragment != null && fragment instanceof ConferencesFragment)
        {
            super.onBackPressed();
        }
        else
        {
            this.replaceFragment(0);
        }
    }

    /**
     * Update the main content by replacing fragments
     * @param position position on fragment in slider drawer
     */
    private void replaceFragment(int position)
    {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction tr = fragmentManager.beginTransaction();

        switch (position)
        {
            case 0:
                tr.replace(R.id.container, new ConferencesFragment());
                break;

            case 1:
                tr.replace(R.id.container, new CreateConferenceFragment());
                break;

            case 2:
                tr.replace(R.id.container, new ProfileFragment());
                break;

            case 3:
                if (this.mConference == null) {
                    return;
                }

                tr.replace(R.id.container, SessionFragment.newInstance(this.mConference.getWebsafeKey()));
                break;
        }

        tr.commit();
    }

    @Override
    public void onAnnouncementReceived(Announcement announcement)
    {
        if (announcement == null)
        {
            Logger.e(TAG, "Announcement is null :(");
            return;
        }

        Logger.d(TAG, announcement.toString());

        Toast.makeText(this, announcement.getMessage(), Toast.LENGTH_LONG).show();
    }
}
