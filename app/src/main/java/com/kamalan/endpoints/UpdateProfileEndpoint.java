package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Profile;
import com.appspot.udacity_gae_conference.conferenceApi.model.ProfileForm;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 24/01/15
 */
public class UpdateProfileEndpoint extends AsyncTask<Void, Void, Profile>
{
    private static final String TAG = UpdateProfileEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private ProgressDialog progressDialog;
    private final Context context;
    private final ProfileForm profileForm;
    private final GetProfileEndpoint.IGetProfileEndpoint listener;

    public UpdateProfileEndpoint(Context context, ProfileForm profileForm,
            GetProfileEndpoint.IGetProfileEndpoint listener)
    {
        this.context = context;
        this.profileForm = profileForm;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected Profile doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            return conferenceApi.saveProfile(profileForm).execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Profile profile)
    {
        progressDialog.dismiss();
        listener.onProfileReceived(profile);
    }

}
