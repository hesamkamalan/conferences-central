package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Profile;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 24/01/15
 */
public class GetProfileEndpoint extends AsyncTask<Void, Void, Profile>
{
    /**
     * ProfileFragment should implement this listener in order to be
     * notified when the response is ready
     */
    public interface IGetProfileEndpoint
    {
        public void onProfileReceived(Profile profile);
    }

    private static final String TAG = GetProfileEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private ProgressDialog progressDialog;
    private Context context;
    private IGetProfileEndpoint listener;

    public GetProfileEndpoint(Context context, IGetProfileEndpoint listener)
    {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected Profile doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            return conferenceApi.getProfile().execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Profile profile)
    {
        progressDialog.dismiss();
        listener.onProfileReceived(profile);
    }

}
