package com.kamalan.endpoints;

import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Announcement;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 25/01/15
 */
public class AnnouncementEndpoint extends AsyncTask<Void, Void, Announcement>
{
    /**
     * ConferencesFragment should implement this listener in order to be
     * notified when the response is ready
     */
    public interface IAnnouncementEndpoint
    {
        public void onAnnouncementReceived(Announcement announcement);
    }

    private static final String TAG = AnnouncementEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private final Context context;
    private final IAnnouncementEndpoint listener;

    public AnnouncementEndpoint(Context context, IAnnouncementEndpoint listener)
    {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected Announcement doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            return conferenceApi.getAnnouncement().execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Announcement announcement)
    {
        listener.onAnnouncementReceived(announcement);
    }

}
