package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.appspot.udacity_gae_conference.sessionApi.SessionApi;
import com.appspot.udacity_gae_conference.sessionApi.model.Session;
import com.appspot.udacity_gae_conference.sessionApi.model.SessionForm;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 14/03/15
 */
public class CreateSessionEndpoint extends AsyncTask<Void, Void, Session>
{
    /**
     * SessionFragment should implement this listener in order to be
     * notified when the response is ready
     */
    public interface ICreateSessionEndpointListener
    {
        public void onSessionCreated(Session session);
    }

    private static final String TAG = CreateSessionEndpoint.class.getSimpleName();

    private static SessionApi sessionApi = null;
    private ProgressDialog progressDialog;
    private final Context context;
    private final String webSafeKey;
    private final SessionForm sessionForm;
    private final ICreateSessionEndpointListener listener;

    public CreateSessionEndpoint(Context context, String webSafeKey, SessionForm sessionForm,
                                 ICreateSessionEndpointListener listener)
    {
        this.context = context;
        this.webSafeKey = webSafeKey;
        this.sessionForm = sessionForm;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected Session doInBackground(Void... params)
    {
        // To just do it once
        if (sessionApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            SessionApi.Builder builder =
                    new SessionApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            sessionApi = builder.build();
        }

        try
        {
            return sessionApi.createSession(webSafeKey, sessionForm).execute();
        }
        catch (IOException e)
        {
            if (!TextUtils.isEmpty(e.getMessage())) {
                Logger.e(TAG, e.getMessage());
            }
            return null;
        }
    }

    @Override
    protected void onPostExecute(Session session)
    {
        progressDialog.dismiss();
        listener.onSessionCreated(session);
    }

}
