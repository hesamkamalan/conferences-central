package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.appspot.udacity_gae_conference.conferenceApi.model.ConferenceQueryForm;
import com.appspot.udacity_gae_conference.conferenceApi.model.Filter;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hesam on 17/01/15
 */
public class ConferencesEndpoint extends AsyncTask<Void, Void, List<Conference>>
{
    /**
     * ConferenceFragment should implement this list in order to be
     * notified when the list is ready
     */
    public interface IConferencesEndpointListener
    {
        public void onConferenceListReceived(List<Conference> conferenceList);
    }

    // Filters on result
    public enum ConferenceFilter {
        ALL, I_CREATED, TO_ATTEND, FILTER_BY_CITY
    }

    private static final String TAG = ConferencesEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private ProgressDialog progressDialog;
    private Context context;
    private ConferenceFilter filter;
    private IConferencesEndpointListener listener;

    private String cityName;

    public ConferencesEndpoint(Context context, ConferenceFilter filter, IConferencesEndpointListener listener)
    {
        this.context = context;
        this.filter = filter;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected List<Conference> doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            switch (this.filter) {
                case ALL:
                    return conferenceApi.queryConferences(null).execute().getItems();
                case I_CREATED:
                    return conferenceApi.getConferencesCreated().execute().getItems();
                case TO_ATTEND:
                    return conferenceApi.getConferencesToAttend().execute().getItems();
                case FILTER_BY_CITY:
                    Filter filter1 = new Filter();
                    filter1.setField("CITY");
                    filter1.setOperator("EQ");
                    filter1.setValue(this.cityName);

                    List<Filter> list = new ArrayList<>(1);
                    list.add(filter1);

                    ConferenceQueryForm queryForm = new ConferenceQueryForm();
                    queryForm.setFilters(list);

                    return conferenceApi.queryConferences(queryForm).execute().getItems();
            }
        }
        catch (IOException | IllegalArgumentException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Conference> conferenceList)
    {
        progressDialog.dismiss();
        listener.onConferenceListReceived(conferenceList);
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }
}
