package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Profile;
import com.appspot.udacity_gae_conference.conferenceApi.model.ProfileForm;
import com.appspot.udacity_gae_conference.conferenceApi.model.WrappedBoolean;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 3/02/15
 */
public class RegistrationEndpoint extends AsyncTask<Void, Void, WrappedBoolean>
{
    public interface IOnRegistrationEndPointListener {
        public void onUserRegisteredForConference(WrappedBoolean status);
        public void onUserUnregisteredFromConference(WrappedBoolean status);
    }

    public enum RegistrationStatus {
        REGISTER,
        UNREGISTER
    }

    private static final String TAG = RegistrationEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private ProgressDialog progressDialog;
    private final Context context;
    private final String webSafeKey;
    private final IOnRegistrationEndPointListener listener;
    private RegistrationStatus registrationStatus;

    public RegistrationEndpoint(Context context, String webSafeKey, RegistrationStatus status,
                                IOnRegistrationEndPointListener listener)
    {
        this.context = context;
        this.webSafeKey = webSafeKey;
        this.listener = listener;
        this.registrationStatus = status;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected WrappedBoolean doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            if (registrationStatus == RegistrationStatus.REGISTER)
                return conferenceApi.registerForConference(webSafeKey).execute();
            else
                return conferenceApi.unregisterFromConference(webSafeKey).execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(WrappedBoolean status)
    {
        progressDialog.dismiss();

        if (registrationStatus == RegistrationStatus.REGISTER)
            listener.onUserRegisteredForConference(status);
        else
            listener.onUserUnregisteredFromConference(status);
    }

}
