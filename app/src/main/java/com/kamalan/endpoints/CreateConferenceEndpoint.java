package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import android.text.TextUtils;
import com.appspot.udacity_gae_conference.conferenceApi.ConferenceApi;
import com.appspot.udacity_gae_conference.conferenceApi.model.Conference;
import com.appspot.udacity_gae_conference.conferenceApi.model.ConferenceForm;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Logger;
import com.kamalan.utility.Storage;

import java.io.IOException;

/**
 * Created by Hesam on 25/01/15
 */
public class CreateConferenceEndpoint extends AsyncTask<Void, Void, Conference>
{
    /**
     * ConferencesFragment should implement this listener in order to be
     * notified when the response is ready
     */
    public interface ICreateConferenceEndpointListener
    {
        public void onConferenceCreated(Conference conference);
    }

    private static final String TAG = CreateConferenceEndpoint.class.getSimpleName();

    private static ConferenceApi conferenceApi = null;
    private ProgressDialog progressDialog;
    private final Context context;
    private final ConferenceForm conferenceForm;
    private final ICreateConferenceEndpointListener listener;

    public CreateConferenceEndpoint(Context context, ConferenceForm conferenceForm,
            ICreateConferenceEndpointListener listener)
    {
        this.context = context;
        this.conferenceForm = conferenceForm;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected Conference doInBackground(Void... params)
    {
        // To just do it once
        if (conferenceApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            ConferenceApi.Builder builder =
                    new ConferenceApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            conferenceApi = builder.build();
        }

        try
        {
            return conferenceApi.createConference(conferenceForm).execute();
        }
        catch (IOException e)
        {
            if (!TextUtils.isEmpty(e.getMessage())) {
                Logger.e(TAG, e.getMessage());
            }
            return null;
        }
    }

    @Override
    protected void onPostExecute(Conference conference)
    {
        progressDialog.dismiss();
        listener.onConferenceCreated(conference);
    }

}
