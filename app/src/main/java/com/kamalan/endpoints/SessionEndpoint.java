package com.kamalan.endpoints;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.appspot.udacity_gae_conference.sessionApi.SessionApi;
import com.appspot.udacity_gae_conference.sessionApi.model.Session;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.kamalan.conference.R;
import com.kamalan.utility.Endpoint;
import com.kamalan.utility.Storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hesam on 09/03/15
 */
public class SessionEndpoint extends AsyncTask<Void, Void, List<Session>>
{
    /**
     * SessionFragment should implement this list in order to be
     * notified when the list is ready
     */
    public interface ISessionsEndpointListener
    {
        void onSessionListReceived(List<Session> sessionList);
        void onSessionDeleted(long sessionId);
    }

    // Filters on result
    public enum SessionFilter
    {
        ALL, FILTER_BY_START_DATE, FILTER_BY_END_DATE, FILTER_BY_TYPE, DELETE
    }

    private static final String TAG = SessionEndpoint.class.getSimpleName();

    private static SessionApi sessionApi = null;
    private ProgressDialog progressDialog;
    private Context context;
    private String conferenceWebSafeKey;
    private SessionFilter filter;
    private long sessionId = 0;
    private ISessionsEndpointListener listener;

    public SessionEndpoint(Context context, String conferenceWebSafeKey,
                           SessionFilter filter, ISessionsEndpointListener listener)
    {
        this.context = context;
        this.conferenceWebSafeKey = conferenceWebSafeKey;
        this.filter = filter;
        this.listener = listener;
    }

    public SessionEndpoint(Context context, long sessionId,
                           SessionFilter filter, ISessionsEndpointListener listener)
    {
        this(context, "", filter, listener);
        this.sessionId = sessionId;
    }

    @Override
    protected void onPreExecute()
    {
        progressDialog = ProgressDialog
                .show(this.context, context.getString(R.string.dialog_loading_title),
                        context.getString(R.string.dialog_loading_wait), true);
    }

    @Override
    protected List<Session> doInBackground(Void... params)
    {
        // To just do it once
        if (sessionApi == null)
        {
            GoogleAccountCredential credential = GoogleAccountCredential.usingAudience(context, Endpoint.AUDIENCE);
            credential.setSelectedAccountName(Storage.getAccountName(context));

            SessionApi.Builder builder =
                    new SessionApi.Builder(Endpoint.HTTP_TRANSPORT, Endpoint.JSON_FACTORY, credential)
                            .setApplicationName(Endpoint.CLIENT_ID);
            sessionApi = builder.build();
        }

        try
        {
            switch (this.filter) {
                case ALL:
                    return sessionApi.querySessions(this.conferenceWebSafeKey, null).execute().getItems();
//                case FILTER_BY_START_DATE:
//                    return conferenceApi.getConferencesCreated().execute().getItems();
//                case FILTER_BY_END_DATE:
//                    return conferenceApi.getConferencesToAttend().execute().getItems();
//                case FILTER_BY_CITY:
//                    Filter filter1 = new Filter();
//                    filter1.setField("CITY");
//                    filter1.setOperator("EQ");
//                    filter1.setValue(this.cityName);
//
//                    List<Filter> list = new ArrayList<>(1);
//                    list.add(filter1);
//
//                    ConferenceQueryForm queryForm = new ConferenceQueryForm();
//                    queryForm.setFilters(list);
//
//                    return conferenceApi.queryConferences(queryForm).execute().getItems();
                case DELETE:
                    if (this.sessionId > 0)
                        sessionApi.deleteSession(this.sessionId).execute();

                    return new ArrayList<Session>();
            }
        }
        catch (IOException | IllegalArgumentException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Session> sessionList)
    {
        progressDialog.dismiss();

        if (this.sessionId <= 0) {
            listener.onSessionListReceived(sessionList);
        }
        else {
            listener.onSessionDeleted(this.sessionId);
        }
    }
}
